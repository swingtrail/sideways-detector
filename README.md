# Sideways Detector

![](https://i.imgur.com/okvhVTh.png)

This indicator script will detect a sideways trend for you.

## How To Use
- Add [Sideways Detector](https://www.tradingview.com/script/zseofnkC-aalfiann-Sideways-Detector/) to Favorite.
- RED ZONE is Sideways, Green Zone is a trend, it can be bullish trend or bearish trend.

---

Author: M ABD AZIZ ALFIAN  
Twitter: [@aalfiann](https://twitter.com/aalfiann)
